#include <stdio.h>

struct Employee
{
    char name[30];
    char doj[30];
    int eid;
    int salary;
};

int main()
{
    struct Employee a;
    printf("Enter the EID, name, date of joining and salary of the employee: \n");
    scanf("%d%s%s%d",&a.eid,&a.name,&a.doj,&a.salary);
    printf("Name: %s\n",a.name);
    printf("Employee ID: %d\n",a.eid);
    printf("Date of joining: %s\n",a.doj);
    printf("Salary: %d\n",a.salary);
    return 0;
}
