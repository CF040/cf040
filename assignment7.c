#include <stdio.h>

int main()
{
    int i,j;
    int t[4];
    int p[3];
    int a[4][3];
    printf("Enter the sales: \n");
	p[0]=0;p[1]=0;p[2]=0;
    for(i=0;i<4;i++)
    {
		t[i]=0;
        for(j=0;j<3;j++)
        {
           scanf("%d",&a[i][j]);
           t[i]+=a[i][j];
           p[j]+=a[i][j];
        }
    }
    for(i=0;i<4;i++)
        printf("Total sales of salesman no.%d is %d\n",i+1,t[i]);
    for(i=0;i<3;i++)
        printf("Total sales of product no.%d is %d\n",i+1,p[i]);
    return 0;
}
