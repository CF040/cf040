#include<stdio.h>

int main()
{
	int n,t;
	printf("Enter the number: ");
	scanf("%d",&n);
	if(n>100 || n<1)//if the input is greater than 100 or lesser than 1, then it is invalid
		printf("Invalid input\n");
	else
	{
		t=0;
		for(int i=1;i<=100;i++)
		{
			t=n*i;
			if(t<=100)//to make sure any multiple does not exceed 100
			printf("%d\n",t);
			else
			break;
		}
	}
	return 0;
}