#include <stdio.h>

int main()
{
    int a,b;
    float divi;
    int *x,*y,sum;
    char ch;
    printf("Enter the two integer: ");
    scanf("%d%d",&a,&b);
    x=&a;
    y=&b;
    printf("Enter the operation you wish to perform:\n");
    printf("Addition(+)\n");
    printf("Subtraction(-)\n");
    printf("Multiplication(*)\n");
    printf("Division(/)\n");
    printf("Enter the symbol: ");
    scanf("%c",&ch);
    scanf("%c",&ch);
    switch(ch)
    {
        case '+': sum=*x+*y;
        printf("The sum is %d",sum);
        break;
        case '-': sum=*x-*y;
        printf("The difference is %d",sum);
        break;
        case '*': sum=(*x)*(*y);
        printf("The product is %d",sum);
        break;
        case '/': divi=(float)(*x)/(*y);
        printf("The quotient is %d",divi);
        break;
        default: printf("Invalid input\n");
    }
    return 0;
}