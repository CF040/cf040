#include<stdio.h>

int main()
{
	float a,b,c,d,e,average;
	printf("Enter the 5 course marks: ");
	scanf("%f%f%f%f%f",&a,&b,&c,&d,&e);
    average=(a+b+c+d+e)/5;
    if(a<40 || b<40 || c<40 || d<40 || e<40)
        printf("Grade= F\n");	
	else if(average>=90 && average<=100)
		printf("Grade= S\n");
	else if(average>=80 && average<=89)
		printf("Grade= A\n");
	else if(average>=70 && average<=79)
		printf("Grade= B\n");
	else if(average>=60 && average<=69)
		printf("Grade= C\n");
	else if(average>=50 && average<=59)
		printf("Grade= D\n");
	else if(average>=40 && average<=49)
		printf("Grade= E\n");
   	return 0;
}