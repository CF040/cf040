#include <stdio.h>

void swap(int *a, int *b)
{
    int temp;
    temp=*a;
    *a=*b;
    *b=temp;
}

int main()
{
    int a,b;
    a=2;
    b=3;
	printf("Before swapping,\n");
	printf("a=%d\n",a);
    printf("b=%d\n",b);
    swap(&a,&b);
	printf("After swapping,\n");
    printf("a=%d\n",a);
    printf("b=%d\n",b);
    return 0;
}
