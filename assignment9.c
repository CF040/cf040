#include <stdio.h>

struct Book
{
    char title[30];
    char author[30];
    int price;
    int pages;
};

int main()
{
    struct Book a,b;
    printf("Enter the title, author name, price and number of pages of first book: \n");
    scanf("%s%s%d%d",&a.title,&a.author,&a.price,&a.pages);
    printf("Enter the title, author name, price and number of pages of second book: \n");
    scanf("%s%s%d%d",&b.title,&b.author,&b.price,&b.pages);
    if(a.price>b.price)
        printf("%s is the expensive book\n",a.title);
    else
        printf("%s is the expensive book\n",b.title);
    if(a.pages<b.pages)
        printf("%s's book is having less pages",a.author);
    else
        printf("%s's book is having less pages",b.author);
    return 0;
}
